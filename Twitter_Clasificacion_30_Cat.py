import os
import re
import string
from cmath import nan

import pandas as pd
#Librería
import psycopg2
import spacy
from autocorrect import Speller  # corrector ortográfico

spell = Speller('es')
#Definición del diccionario se utiliza el más extenso
nlp = spacy.load('es_core_news_lg')
import numpy as np
import unidecode
#Stop Words de es_core_news_sm
from spacy.lang.es.stop_words import STOP_WORDS

# ### Credenciales para establecer conexión con la base de datos

def establecer_conexion():
	#Estableciendo la conexión con la BBDD PostgreSQL
	conn = psycopg2.connect(
	database="venezuela", 
		user='venezuela', 
		password='basededatosvenezuela2021', 
		host='192.168.1.113', 
		port= '5432')

	return conn

def obtener_ultima_fecha():
	conn = establecer_conexion()
	cursor = conn.cursor()

	cursor.execute("select max(fecha) from db_categorias")
	fecha_referencia = cursor.fetchall()[0]

	fecha_referencia = fecha_referencia[0].strftime('%Y-%m-%d')

	return fecha_referencia


def obtener_cod_municipios():
	conn = establecer_conexion()
	cursor = conn.cursor()

	fecha_referencia = obtener_ultima_fecha()

	print("La ultima fecha_referencia es ", fecha_referencia, type(fecha_referencia))

	cursor.execute("select distinct codigo_mun from tweets", ())
	aux = cursor.fetchall()

	return aux


#Función para remover hashtags y enlaces
def strip_links(text):
	link_regex	= re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
	links		 = re.findall(link_regex, text)
	for link in links:
		text = text.replace(link[0], ', ')	
	return text

def strip_all_entities(text):
	entity_prefixes = ['@','#']
	for separator in  string.punctuation:
		if separator not in entity_prefixes :
			text = text.replace(separator,' ')
	words = []
	for word in text.split():
		word = word.strip()
		if word:
			if word[0] not in entity_prefixes:
				words.append(word)
	return ' '.join(words)



#Función para limpiar los datos
def text_cleaning(sentence, stopwords_spacy, puntua):
	doc = nlp(sentence)
	tokens = []
	for token in doc:
		if token.lemma_ != "-PRON-":
			temp = token.lemma_.strip()
		else:
			temp = token
		tokens.append(temp)
	clean_tokens = []
	for token in tokens:
		if token not in stopwords_spacy and token not in puntua:
			clean_tokens.append(token)
	return clean_tokens




#función para ordenas los datos de acuerdo a su categoría (0, 1, 2, 3,...), fecha, coordenadas, índice y contenido del Tweet

def preparar(datos, clasificacion, emoji_pattern, punt, stopwords_spacy):
	indice = []
	texto = []
	coord_x = []
	coord_y = []
	fecha = []
	id_mun = []
	categoria = []

	for i in range(0, len(datos)):
		if datos[i][15] == clasificacion:
			indice.append(i)
			fecha.append(datos[i][2])
			text_limp = text_cleaning(spell(emoji_pattern.sub(r'', strip_all_entities(datos[i][1]))).lower(), stopwords_spacy, punt)
			texto.append(unidecode.unidecode(str(text_limp)))
			coord_x.append(datos[i][17])
			coord_y.append(datos[i][18])
			id_mun.append(datos[i][22])
			categoria.append(clasificacion)
	df_clas = pd.DataFrame([indice, id_mun, fecha, texto, coord_x, coord_y, categoria]).T
	df_clas.columns = ['indice', 'ID', 'fecha', 'Tweet', 'coord_X', 'coord_Y', 'categoria']
	return df_clas	


def pattern_searcher(search_str:str, search_list:str):
	search_str = search_str.replace("'", "")
	search_str = search_str.replace("[", "")
	search_str = search_str.replace("]", "")
	lista = search_str.split(",")

	solu = nan
	for i in range(0, len(search_list)):
		if("\n" in search_list[i]):
			search_list[i] = search_list[i][0:len(search_list[i])-1]
		if search_list[i] in lista:
				solu = 1

	return solu



def realizar_categorizacion():
	conn = establecer_conexion()
	fecha_referencia = obtener_ultima_fecha()
	aux = obtener_cod_municipios()

	contador = 1

	for i in range(0, len(aux)):
		print("Municipio ", aux[i], " añadiendose...")
		
		print("Se ejecuta la iteracción ", contador, "/",len(aux))
		
		sql_select_Query2 = "select * from temas_tweets"
		cursor = conn.cursor()
		cursor.execute(sql_select_Query2)


		cursor.execute("select * from tweets WHERE codigo_mun = '" + str(aux[i][0]) + "' AND created_at > '" + str(fecha_referencia) +"';")
		records = cursor.fetchall()


		#Ejecutando una función con el método MySQL()
		cursor.execute("select version()")


		#Creando un objeto cursor utilizando el método cursor()
		cursor = conn.cursor()


		#Definición de los códigos de emojis que serán eliminados
		emoji_pattern = re.compile("["
				u"\U0001F600-\U0001F64F"  # emoticons
				u"\U0001F300-\U0001F5FF"  # symbols & pictographs
				u"\U0001F680-\U0001F6FF"  # transport & map symbols
				u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
					"]+", flags=re.UNICODE)


		#Incorporación de palabras vacías para evitar errores

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/palabrasvacias.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			STOP_WORDS.add(linea)

		#Construcción de lista de palabras vacias
		stopwords_spacy = list(STOP_WORDS)

		#Remoción de signos de puntuación 
		puntua = string.punctuation + '¡¿'







		#Selección de una muestra de 10 mil Tweets para realizar las pruebas
		records_filt = records[:5000]



		#Organización de la información de acuerdo a la fecha, coordenadas, indice y contenido del Tweet

		otros = preparar(records_filt, 0, emoji_pattern, puntua, stopwords_spacy)
		riesgos_nat = preparar(records_filt, 1, emoji_pattern, puntua, stopwords_spacy)
		violencia = preparar(records_filt, 2, emoji_pattern, puntua, stopwords_spacy)
		migracion = preparar(records_filt, 3, emoji_pattern, puntua, stopwords_spacy)
		serv_pub = preparar(records_filt, 4, emoji_pattern, puntua, stopwords_spacy)
		viol_genero = preparar(records_filt, 5, emoji_pattern, puntua, stopwords_spacy)
		ayuda_salud = preparar(records_filt, 6, emoji_pattern, puntua, stopwords_spacy)
		politica = preparar(records_filt, 7, emoji_pattern, puntua, stopwords_spacy)
		deportes = preparar(records_filt, 8, emoji_pattern, puntua, stopwords_spacy)
		ocio = preparar(records_filt, 9, emoji_pattern, puntua, stopwords_spacy)



		#Listas con los patrones que serán clasificados a partir del grupo riesgos naturales

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/riesgosNaturales/lluvia.txt"
		fichero = open(ruta, 'r')

		lluvia = []
		for linea in fichero:
			lluvia.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/riesgosNaturales/inundacion.txt"
		fichero = open(ruta, 'r')

		inundacion = []
		for linea in fichero:
			inundacion.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/riesgosNaturales/terremoto.txt"
		fichero = open(ruta, 'r')

		terremoto = []
		for linea in fichero:
			terremoto.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/riesgosNaturales/huracan.txt"
		fichero = open(ruta, 'r')

		huracan = []
		for linea in fichero:
			huracan.append(linea)


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/riesgosNaturales/incendio.txt"
		fichero = open(ruta, 'r')

		incendio = []
		for linea in fichero:
			incendio.append(linea)




		#Listas con los patrones que serán clasificados a partir del grupo servicios públicos
		serv_elect = []
		serv_agua = []
		serv_int = []
		serv_gas = []
		serv_combust = []

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/serviciosPublicos/servicioElectrico.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			serv_elect.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/serviciosPublicos/servicioAgua.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			serv_agua.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/serviciosPublicos/servicioInternet.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			serv_int.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/serviciosPublicos/servicioGas.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			serv_gas.append(linea)


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/serviciosPublicos/servicioCombustible.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			serv_combust.append(linea)




		#Listas con los patrones que serán clasificados a partir del grupo violencia
		viol_robo = []
		viol_delincuencia = []
		viol_homicidio = []
		viol_drogas = []
		viol_secuest = []

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violencia/robo.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			viol_robo.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violencia/delincuencia.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			viol_delincuencia.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violencia/homicidio.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			viol_homicidio.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violencia/drogas.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			viol_drogas.append(linea)


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violencia/secuestro.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			viol_secuest.append(linea)




		#Listas con los patrones que serán clasificados a partir del grupo salud-ayudas

		salud_covid = []
		salud_cancer = []
		salud_sangre = []
		salud_desnut = []
		salud_medicina = []
		salud_dinero = []

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/saludAyudas/covid.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			salud_covid.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/saludAyudas/cancer.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			salud_cancer.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/saludAyudas/sangre.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			salud_sangre.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/saludAyudas/desnut.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			salud_desnut.append(linea)


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/saludAyudas/medicina.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			salud_medicina.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/saludAyudas/dinero.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			salud_dinero.append(linea)




		#Listas con los patrones que serán clasificados a partir del grupo violencia de genero

		v_genero_acoso = []
		v_genero_muerte = []
		v_genero_viola = []
		v_genero_prostit = []


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violenciaGenero/acoso.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			v_genero_acoso.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violenciaGenero/muerte.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			v_genero_muerte.append(linea)


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violenciaGenero/viola.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			v_genero_viola.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/violenciaGenero/prostitucion.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			v_genero_prostit.append(linea)




		#Listas con los patrones que serán clasificados a partir del grupo migración

		migra_refugiado = []
		migra_document = []
		migra_escape = []
		migra_xenofo = []
		migra_control = []

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/migracion/refugiado.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			migra_refugiado.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/migracion/documentacion.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			migra_document.append(linea)


		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/migracion/escape.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			migra_escape.append(linea)
			
			
		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/migracion/xenofobia.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			migra_xenofo.append(linea)
			

		ruta = "/home/gis4tech/GIS4TECH/ACH_VENEZUELA/2_ACTIVIDADES/PT1_ESTIMACION/PT1_A09_P03_BBDD_secundaria_dinamica/Desarrollo_Descarga_Info_RRSS/Notebooks_Twitter_30_categorias/ficheros/migracion/control.txt"
		fichero = open(ruta, 'r')
		for linea in fichero:
			migra_control.append(linea)


		#Función para bucar patrones en un dataframe

		



		#Definición de patrones desastres naturales
		pattern1 = (lluvia)
		pattern2 = (inundacion)
		pattern3 = (terremoto)
		pattern4 = (huracan)
		pattern5 = (incendio)

		#Definición de patrones fallo de servicios					
		pattern6 = (serv_elect)
		pattern7 = (serv_agua)
		pattern8 = (serv_int)
		pattern9 = (serv_gas)
		pattern10 = (serv_combust)

		#Definición de patrones delincuencia					
		pattern11 = (viol_robo)
		pattern12 = (viol_delincuencia)
		pattern13 = (viol_homicidio)
		pattern14 = (viol_drogas)
		pattern15 = (viol_secuest)

		#Definición de patrones ayuda-salud
		pattern16 = (salud_covid)
		pattern17 = (salud_cancer)
		pattern18 = (salud_sangre)
		pattern19 = (salud_desnut)
		pattern20 = (salud_medicina)
		pattern21 = (salud_dinero)

		#Definición de patrones de violencia de género
		pattern22 = (v_genero_acoso)
		pattern23 = (v_genero_muerte)
		pattern24 = (v_genero_viola)
		pattern25 = (v_genero_prostit)

		#Definición de patrones de migración
		pattern26 = (migra_refugiado)
		pattern27 = (migra_document)
		pattern28 = (migra_escape)
		pattern29 = (migra_xenofo)
		pattern30 = (migra_control)


		# ### Clasificación (30 categorías)
		# En la base de datos existen aproximadamente 2 millones de tweets, por lo cual será necesario categorizar los twests existentes y posteriormente programar la categorización de los tweets que se almacenan periodicamente. 



		#Construcción de columnas con la clasificación del patrón para desastres naturales
		riesgos_nat['lluvia'] = riesgos_nat['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern1))
		riesgos_nat['inundacion'] = riesgos_nat['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern2))
		riesgos_nat['terremoto'] = riesgos_nat['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern3))
		riesgos_nat['huracan'] = riesgos_nat['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern4))
		riesgos_nat['incendio'] = riesgos_nat['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern5))

		#Construcción de columnas con la clasificación del patrón para  fallo de servicios
		serv_pub['fallo_electrico'] = serv_pub['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern6))
		serv_pub['sin_agua'] = serv_pub['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern7))
		serv_pub['sin_internet'] = serv_pub['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern8))
		serv_pub['sin_gas_nat'] = serv_pub['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern9))
		serv_pub['sin_gasolina'] = serv_pub['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern10))

		#Construcción de columnas con la clasificación del patrón para  violencia - delincuencia
		violencia['robo'] = violencia['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern11))
		violencia['delincuencia'] = violencia['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern12))
		violencia['homicidio'] = violencia['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern13))
		violencia['drogas'] = violencia['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern14))
		violencia['secuestro'] = violencia['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern15))

		#Construcción de columnas con la clasificación del patrón para  ayuda-salud
		ayuda_salud['covid19'] = ayuda_salud['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern16))
		ayuda_salud['cancer'] = ayuda_salud['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern17))
		ayuda_salud['donar_sangre'] = ayuda_salud['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern18))
		ayuda_salud['desnutricion'] = ayuda_salud['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern19))
		ayuda_salud['donar_medicina'] = ayuda_salud['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern20))
		ayuda_salud['colab_economica'] = ayuda_salud['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern21))

		#Construcción de columnas con la clasificación del patrón para  violencia de género
		viol_genero['acoso'] = viol_genero['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern22))
		viol_genero['homicidio'] = viol_genero['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern23))
		viol_genero['violacion'] = viol_genero['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern24))
		viol_genero['prostitucion'] = viol_genero['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern25))

		#Construcción de columnas con la clasificación del patrón para  migración
		migracion['refugiado'] = migracion['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern26))
		migracion['documentos'] = migracion['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern27))
		migracion['escape'] = migracion['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern28))
		migracion['racismo'] = migracion['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern29))
		migracion['control_mig'] = migracion['Tweet'].apply(lambda x: pattern_searcher(search_str=x, search_list=pattern30))




		#Construcción de dataframe conjunto con sus respectivas clasificaciones
		df_clasificado = pd.concat([riesgos_nat, serv_pub, violencia, ayuda_salud, viol_genero, migracion]).set_index('indice')


		cursor = conn.cursor()
		
		array_clasificado = (df_clasificado.to_numpy())

		if(len(array_clasificado) != 0):
			fecha = str(array_clasificado[0][1]).split(" ")
			fecha_add = fecha[0]

			for j in range(0, len(array_clasificado)):
				try:
					cursor.execute('''INSERT INTO db_categorias(indice, id, fecha, tweet, \"cordX\", \"cordY\", categoria,
					lluvia, inundacion, terremoto,huracan, incendio, fallo_electrico, sin_agua, sin_internet, sin_gas_nat, sin_gasolina,
					robo, delincuencia, homicidio, drogas, secuestro, covid19, cancer, donar_sangre, desnutricion, donar_medicina,
					colab_economica, acoso, violacion, prostitucion, refugiado, documentos, escape, racismo, control_mig)
					VALUES (nextval('seq_db_categorias'), (%s), (%s), (%s), (%s), (%s),
					(%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s),
					(%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s),
					(%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s), (%s)
					);''',
					(array_clasificado[j][0], fecha_add, array_clasificado[j][2], array_clasificado[j][3], array_clasificado[j][4],

					array_clasificado[j][5],array_clasificado[j][6],array_clasificado[j][7],array_clasificado[j][8],array_clasificado[j][9],
					array_clasificado[j][10],array_clasificado[j][11],array_clasificado[j][12],array_clasificado[j][13],array_clasificado[j][14],

					array_clasificado[j][15],array_clasificado[j][16],array_clasificado[j][17],array_clasificado[j][18],array_clasificado[j][19],
					array_clasificado[j][20],array_clasificado[j][21],array_clasificado[j][22],array_clasificado[j][23],array_clasificado[j][24],
					
					array_clasificado[j][25],array_clasificado[j][26],array_clasificado[j][27],array_clasificado[j][28],array_clasificado[j][29],
					array_clasificado[j][30],array_clasificado[j][31],array_clasificado[j][32],array_clasificado[j][33],array_clasificado[j][34],))
			
				except Exception as ex:
					print("Excepcion", ex)

				conn.commit()
		contador = contador + 1

def eliminar_filas_erroneas():
	conn = establecer_conexion()
	cursor = conn.cursor()


	cursor.execute("DELETE FROM db_categorias WHERE ((tweet = 'nada') OR (tweet = '[]'))" )

	conn.commit()

if __name__ == "__main__":
	realizar_categorizacion()
	eliminar_filas_erroneas()
